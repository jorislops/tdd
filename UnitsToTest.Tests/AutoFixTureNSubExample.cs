using AutoFixture;
using AutoFixture.AutoNSubstitute;
using AutoFixture.NUnit3;
using NSubstitute;
using NUnit.Framework;

namespace UnitsToTest.Tests
{
    public class AutoFixTureNSubExample
    {
        [Test]
        public void TestFixture1()
        {
            //Arrange
            var fixture = new Fixture();
            int value = fixture.Create<int>();
            var logger = Substitute.For<IMyLogger>();
            var sut = new Calculator(logger);
            
            //Act
            sut.Subtract(value);
            
            //Assert
            logger.Received().Write(Arg.Any<string>());
            Assert.AreEqual(0 - value, sut.Value);
        }
        
        [Test, AutoData]
        public void TestFixture2(int value)
        {
            var logger = Substitute.For<IMyLogger>();
            
            var sut = new Calculator(logger);
            sut.Subtract(value);
            
            logger.Received().Write(Arg.Any<string>());
            Assert.AreEqual(0 - value, sut.Value);
        }        

        [Test, AutoNSubstituteData]
        public void TestFixture3(int value, 
            [Frozen] IMyLogger logger, 
            Calculator sut )
        {
            sut.Subtract(value);
            logger.Received().Write(Arg.Any<string>());
            Assert.AreEqual(0 - value, sut.Value);
        }    
        
        public class AutoNSubstituteDataAttribute : AutoDataAttribute
        {
            public AutoNSubstituteDataAttribute() 
                : base(new Fixture()
                    .Customize(new AutoNSubstituteCustomization()))
            {
            }
        }
        
        //See for example https://gist.github.com/skalinets/4571557
        
        
        
    }
}