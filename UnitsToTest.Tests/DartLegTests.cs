using System;
using NSubstitute;
using NUnit.Framework;


namespace UnitsToTest.Tests
{
    public class DartLegTests
    {
        [Test]
        public void ThrowResult_NormalScore_CorrectScore()
        {
            var player1Counter = Substitute.For<IDartCounter>();
            player1Counter.Throw(Arg.Any<int>(), Arg.Any<int>()).Returns(
                DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue,
                DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue,
                DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Dead
            );
            player1Counter.Score.Returns(501 - 60, 501 - 120, 501 - 180
                , 321-60, 321-120, 321-180
                , 141-60, 141-120, 21);
            
            var player2Counter = Substitute.For<IDartCounter>();
            player2Counter.Throw(Arg.Any<int>(), Arg.Any<int>()).Returns(                
                DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue,
                DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue,
                DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Continue, DartCounter.ThrowResult.Win
            );
            
            //player2Counter.Throw(20, 3).Returns(DartCounter.ThrowResult.Continue);
            player2Counter.Score.Returns(
                501 - 60, 501 - 120, 501 - 180
                , 321-60, 321-120, 321-180
                , 141-19*3, 141-19*3-16*3, 0  
                );
            
            var sut = new DartLeg(new[]{player1Counter, player2Counter});

            //first turn - player 1
            for (int i = 0; i < 3; i++)
            {
                var throwResult = sut.Throw(20, 3);
                Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
                Assert.AreEqual(501 - (i+1) * 60, sut.Player1Score);
            }
            
            //second turn - player 2
            for (int i = 0; i < 3; i++)
            {
                var throwResult = sut.Throw(20, 3);
                Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
                Assert.AreEqual(501 - (i+1) * 60, sut.Player2Score);
            }
            
            //3rd turn - player 1
            for (int i = 0; i < 3; i++)
            {
                var throwResult = sut.Throw(20, 3);
                Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
                Assert.AreEqual(321 - (i+1) * 60, sut.Player1Score);
            }
            
            //4th turn - player 2
            for (int i = 0; i < 3; i++)
            {
                var throwResult = sut.Throw(20, 3);
                Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
                var player2Score = sut.Player2Score;
                Assert.AreEqual(321 - (i+1) * 60, player2Score);
            }
            
            //5th turn - player 1 is dead 21 points left
            for (int i = 0; i < 2; i++)
            {
                var throwResult = sut.Throw(20, 3);
                Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
                Assert.AreEqual(141 - (i+1) * 60, sut.Player1Score);
            }
            {
                var throwResult = sut.Throw(20, 3);
                Assert.AreEqual(DartCounter.ThrowResult.Dead, throwResult);
                Assert.AreEqual(21, sut.Player1Score);
            }
            
            //6th turn - player 2 wins 
            {
                var throwResult = sut.Throw(19, 3);
                Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
                Assert.AreEqual(84, sut.Player2Score);
                
                throwResult = sut.Throw(16, 3);
                Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
                Assert.AreEqual(36, sut.Player2Score);
                
                throwResult = sut.Throw(18, 2);
                Assert.AreEqual(DartCounter.ThrowResult.Win, throwResult);
                Assert.AreEqual(0, sut.Player2Score);
            }
        }
        
        [Test]
        public void PlayGame()
        {
            var sut = new DartLeg(new[]
            {
                new DartCounter(),
                new DartCounter(),
            });

            Assert.AreEqual(3, sut.ArrowCount);
            Assert.AreEqual(501, sut.Player1Score);
            Assert.AreEqual(501, sut.Player2Score);

            var throwResult = sut.Throw(20, 3);
            Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
            Assert.AreEqual(2, sut.ArrowCount);
            Assert.AreEqual(501 - 60, sut.Player1Score);
            Assert.AreEqual(501, sut.Player2Score);

            throwResult = sut.Throw(20, 3);
            Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
            Assert.AreEqual(1, sut.ArrowCount);
            Assert.AreEqual(501 - 120, sut.Player1Score);
            Assert.AreEqual(501, sut.Player2Score);

            throwResult = sut.Throw(20, 3);
            Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
            Assert.AreEqual(501 - 180, sut.Player1Score);
            Assert.AreEqual(501, sut.Player2Score);


            Assert.AreEqual(3, sut.ArrowCount);
            Assert.IsTrue(sut.PlayerTurn == DartLeg.ePlayerTurn.Player2);

            throwResult = sut.Throw(20, 3);
            Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
            Assert.AreEqual(501 - 180, sut.Player1Score);
            Assert.AreEqual(501 - 60, sut.Player2Score);

            throwResult = sut.Throw(20, 3);
            Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
            Assert.AreEqual(501 - 180, sut.Player1Score);
            Assert.AreEqual(501 - 120, sut.Player2Score);

            throwResult = sut.Throw(20, 3);
            Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
            Assert.AreEqual(501 - 180, sut.Player1Score);
            Assert.AreEqual(501 - 180, sut.Player2Score);

            Assert.AreEqual(3, sut.ArrowCount);
            Assert.IsTrue(sut.PlayerTurn == DartLeg.ePlayerTurn.Player1);
        }
    }
}