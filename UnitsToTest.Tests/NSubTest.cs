using System;
using NSubstitute;
using NUnit.Framework;

namespace UnitsToTest.Tests
{
    public interface ICalculator
    {
        int Add(int a, int b);
        string Mode { get; set; }
        event EventHandler PoweringUp;
    }
    
    public class NSubTest
    {
        [Test]
        public void TestCal()
        {
            var calculator = Substitute.For<ICalculator>();
            calculator.Add(Arg.Any<int>(), Arg.Any<int>()).Returns(3);
            
            Assert.That(calculator.Add(1,2), Is.EqualTo(3));

            //calculator.Add(1, 2);


            calculator.Mode.Returns("DESC");
            Assert.That(calculator.Mode, Is.EqualTo("DESC"));

            //calculator.DidNotReceiveWithAnyArgs().Add(5, 3);
            //calculator.Add(10, -5);
            //calculator.Received().Add(10, Arg.Any<int>());
            //calculator.Received().Add(10, Arg.Is<int>(x => x < 0));

            calculator.Add(1, 2);
            
            
            
            
            Assert.That(calculator.Add(-2, 5), Is.EqualTo(3));
            
            calculator.Received().Add(1, -2);
//            
//            calculator.Received().Add(-3, 3);

            //calculator.Received().Add(Arg.Any<int>(), Arg.Is<int>(x => x < 0));
        }
        
    }
}