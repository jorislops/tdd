using System;
using NUnit.Framework;

namespace UnitsToTest.Tests
{
    public class DartCounterTest
    {
        [Test]
        public void DartCounter_Initialize_ScoreIs501()
        {
            //arrange
            var sut = new DartCounter();
            
            //Act
            var result = sut.Score;
            
            //Assert
            Assert.AreEqual(501, result);
        }

        [TestCase(20)]
        [TestCase(1)]
        public void DartCounter_ThrowNotEndGame_ScoreSubtractedFrom501(int value)
        {
            //Arrange
            var sut = new DartCounter();

            //Act
            sut.Throw(value);
            
            //Assert
            var result = sut.Score;
            Assert.AreEqual(501 - value, result);
        }

        [TestCase(20, 4)]
        [TestCase(1, 0)]
        public void DartCounter_InvalidMultiplier_ThrowsArgumentException(int value, int multiplier)
        {
            //Arrange
            var sut = new DartCounter();
            
            //Act
            Assert.Catch<ArgumentException>(() => { sut.Throw(value, multiplier); });
        }

        [TestCase(20, 3, 441)]
        [TestCase(10, 2, 481)]
        public void DartCounter_ThrowNotEndGameWithMultiplier_ScoreTimesMultiplierSubractedFrom501(
            int score, int multiplier, int expected)
        {
            //Arrange
            var sut = new DartCounter();

            //Act
            var throwResult = sut.Throw(score, multiplier);

            //Assert
            var result = sut.Score;
            Assert.AreEqual(expected, result);
            Assert.AreEqual(DartCounter.ThrowResult.Continue, throwResult);
        }

        [TestCase(18, 10, 2)]
        [TestCase(7, 10, 2)]
        public void DartCounter_ThrowWithUnderZero_Dead(int initValue, int score, int multiplier)
        {
            var sut = new DartCounter(initValue);

            //Act
            DartCounter.ThrowResult result = sut.Throw(score, multiplier);
            
            //Assert
            Assert.AreEqual(DartCounter.ThrowResult.Dead, result);
            Assert.AreEqual(initValue, sut.Score);
        }
        
        [TestCase(18, 9, 2)]
        [TestCase(14, 7, 2)]
        public void DartCounter_ZeroScoreWithDouble_Win(int initValue, int score, int multiplier)
        {
            var sut = new DartCounter(initValue);

            //Act
            DartCounter.ThrowResult result = sut.Throw(score, multiplier);
            
            //Assert
            Assert.AreEqual(DartCounter.ThrowResult.Win, result);
            Assert.AreEqual(0, sut.Score);
        }
        
        [TestCase(50, 50, 1)]
        public void DartCounter_ZeroScoreWithBullEye_Win(int initValue, int score, int multiplier)
        {
            var sut = new DartCounter(initValue);

            //Act
            DartCounter.ThrowResult result = sut.Throw(score, multiplier);
            
            //Assert
            Assert.AreEqual(DartCounter.ThrowResult.Win, result);
            Assert.AreEqual(0, sut.Score);
        }
        
        [TestCase(18, 18, 1)]
        [TestCase(14, 14, 1)]
        public void DartCounter_ZeroScoreWithNotADouble_ScoreUnchangedAndDead(int initValue, int score, int multiplier)
        {
            var sut = new DartCounter(initValue);

            //Act
            DartCounter.ThrowResult result = sut.Throw(score, multiplier);
            
            //Assert
            Assert.AreEqual(DartCounter.ThrowResult.Dead, result);
            Assert.AreEqual(initValue, sut.Score);
        }
        
        [TestCase(18, 17, 1)]
        [TestCase(15, 7, 2)]
        public void DartCounter_One_ScoreUnchangedAndDead(int initValue, int score, int multiplier)
        {
            var sut = new DartCounter(initValue);

            //Act
            DartCounter.ThrowResult result = sut.Throw(score, multiplier);
            
            //Assert
            Assert.AreEqual(DartCounter.ThrowResult.Dead, result);
            Assert.AreEqual(initValue, sut.Score);
        }
        

        [Test]
        public void DartCounter_TestThrowsContinueAndNext_TwoThrowsContinueThenNextPlayer()
        {
            var sut = new DartCounter();

            Assert.AreEqual(DartCounter.ThrowResult.Continue, sut.Throw(10, 1));
            Assert.AreEqual(DartCounter.ThrowResult.Continue, sut.Throw(10, 1));
            Assert.AreEqual(DartCounter.ThrowResult.Continue, sut.Throw(10, 1));
            
            Assert.AreEqual(DartCounter.ThrowResult.Continue, sut.Throw(10, 1));
            Assert.AreEqual(DartCounter.ThrowResult.Continue, sut.Throw(10, 1));
            Assert.AreEqual(DartCounter.ThrowResult.Continue, sut.Throw(10, 1));
        }
    }
}