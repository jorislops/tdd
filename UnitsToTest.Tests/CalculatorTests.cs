using AutoFixture;
using NUnit.Framework.Internal;
using Xunit;

namespace UnitsToTest.Tests
{
    public class CalculatorTests
    {
        [Fact]
        public void Tradition()
        {
            //Arrange
            var sut = new Calculator(new MyConsoleLogger());
            
            //Act
            sut.Subtract(1);
            
            //Assert
            Assert.True(sut.Value < 0);
        }
        
        [Fact]
        public void Manual_Anonymous_Data() 
        {
            //Arrange
            var sut = new Calculator(new MyConsoleLogger());
            var anoymouseData = 394;
            
            //Act
            sut.Subtract(anoymouseData);
            
            //Assert
            Assert.True(sut.Value < 0);
        }

        [Fact]
        public void AutoFixture_Anoymous_Data()
        {
            //Arrange
            var fixture = new Fixture();
            var sut = new Calculator(fixture.Create<MyConsoleLogger>());
            
            //Act
            sut.Subtract(fixture.Create<int>());
            
            //Assert
            Assert.True(sut.Value < 0);
        }
        
    }
    

}