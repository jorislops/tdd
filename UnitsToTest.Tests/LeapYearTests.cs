﻿using System;
using System.Linq;
using NUnit.Framework;
using UnitsToTest;

namespace UnitsToTest.Tests
{
    public class LeapYearTests
    {
        [TestCase(1804)]
        [TestCase(1808)]
        [TestCase(1812)]
        public void IsLeapYear_DividableByFour_True(int value)
        {
            //Arrange
            LeapYear leapYear = new LeapYear();

            //Act
            bool result = leapYear.IsLeapYear(value);

            //Verify
            Assert.IsTrue(result);
        }

        [TestCase(1800)]
        [TestCase(1900)]
        public void IsLeapYear_DividableByHundred_False(int value)
        {
            LeapYear leapYear = new LeapYear();

            bool result = leapYear.IsLeapYear(value);

            Assert.That(result, Is.False);
        }

        [TestCase(1600)]
        [TestCase(2000)]
        public void IsLeapYear_DividableByFourHundred_True(int value)
        {
            LeapYear leapYear = new LeapYear();

            bool result = leapYear.IsLeapYear(value);

            Assert.That(result, Is.True);
        }

        [Test]
        public void IsLeapYear_TestAllYears()
        {
            int[] leapYears =
            {
                1804, 1904, 2004, 2104, 2204, 2304, 1808, 1908, 2008, 2108, 2208, 2308, 1812, 1912, 2012, 2112, 2212,
                2312, 1816, 1916, 2016, 2116, 2216, 2316,
                1820, 1920, 2020, 2120, 2220, 2320, 1824, 1924, 2024, 2124, 2224, 2324, 1828, 1928, 2028, 2128, 2228,
                2328,
                1832, 1932, 2032, 2132, 2232, 2332, 1836, 1936, 2036, 2136, 2236, 2336, 1840, 1940, 2040, 2140, 2240,
                2340,
                1844, 1944, 2044, 2144, 2244, 2344, 1848, 1948, 2048, 2148, 2248, 2348, 1852, 1952, 2052, 2152, 2252,
                2352,
                1856, 1956, 2056, 2156, 2256, 2356, 1860, 1960, 2060, 2160, 2260, 2360, 1864, 1964, 2064, 2164, 2264,
                2364,
                1868, 1968, 2068, 2168, 2268, 2368, 1872, 1972, 2072, 2172, 2272, 2372, 1876, 1976, 2076, 2176, 2276,
                2376,
                1880, 1980, 2080, 2180, 2280, 2380, 1884, 1984, 2084, 2184, 2284, 2384, 1888, 1988, 2088, 2188, 2288,
                2388,
                1892, 1992, 2092, 2192, 2292, 2392, 1896, 1996, 2096, 2196, 2296, 2396, 2000, 2400
            };

            LeapYear leapYear = new LeapYear();
            
            for (int i = 1800; i <= 2400; i++)
            {
                bool isLeapYear = leapYear.IsLeapYear(i);
                if (leapYears.Contains(i))
                {
                    Assert.IsTrue(isLeapYear);    
                }
                else
                {
                    Assert.IsFalse(isLeapYear);
                }
            }
        }
    }
}