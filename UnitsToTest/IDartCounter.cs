namespace UnitsToTest
{
    public interface IDartCounter
    {
        int Score { get; }
        DartCounter.ThrowResult Throw(int value, int multiplier = 1);
    }
}