﻿using System;

namespace UnitsToTest
{
    public class LeapYear
    {
        public bool IsLeapYear(int year)
        {
            //refactor 2
//          return year % 400 == 0 || year % 100 != 0 && year % 4 == 0;

            //refactor 1
//          return year % 400 == 0 ? true : 
//                (year % 100 == 0 ? false:
//                    (year % 4 == 0 ? true : false));            
            
            if (year % 400 == 0)
            {
                return true;
            }

            if (year % 100 == 0)
            {
                return false;
            }
            
            if (year % 4 == 0)
            {
                return true;
            }

            return false;
        }
    }
}