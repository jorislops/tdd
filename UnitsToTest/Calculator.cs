using System;
using Castle.Core.Logging;

namespace UnitsToTest
{
    public class Calculator
    {
        private readonly IMyLogger _logger;
        public Calculator(IMyLogger logger)
        {
            _logger = logger;
        }
        
        public int Value { get; private set; }

        public void Subtract(int number)
        {
            Value -= number;
            _logger.Write($"New Value After Subtraction {Value}");
        }
    }

    public interface IMyLogger
    {
        void Write(string text);
    }

    public class MyConsoleLogger : IMyLogger
    {
        public void Write(string text)
        {
            Console.WriteLine(text);
        }
    }
}