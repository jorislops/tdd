using System;

namespace UnitsToTest
{
    public class DartCounter : IDartCounter
    {
        public DartCounter(int initValue = 501)
        {
            Score = initValue;
        }

        public int Score { get; private set; }

        public enum ThrowResult
        {
            Dead, Win, Continue
        }
        
        public ThrowResult Throw(int value, int multiplier = 1)
        {
            if (!(value >= 1 && value <= 20 || value == 25 || value == 50))
                throw new ArgumentException();
            if (multiplier < 1 || multiplier > 3)
                throw new ArgumentException();

            int score = Score - (value * multiplier);

            if (score == 1 || score < 0)
            {
                return ThrowResult.Dead;
            }

            if (score == 0)
            {
                if (value == 50)
                {
                    Score = 0;
                    return ThrowResult.Win;                    
                }
                
                if (multiplier != 2)
                {
                    return ThrowResult.Dead;
                }
                
                Score = 0;
                return ThrowResult.Win;
            }

            Score = score;
            return ThrowResult.Continue;
        }
    }
}