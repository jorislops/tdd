using System;

namespace UnitsToTest
{
    public class DartLeg
    {
        public enum ePlayerTurn
        {
            Player1, Player2
        }

        private readonly IDartCounter[] _players;
        private IDartCounter _currentPlayer;
        
        public int ArrowCount { get; private set; } = 3;

        public int Player1Score => _players[0].Score;

        public int Player2Score => _players[1].Score;

        public ePlayerTurn PlayerTurn => _currentPlayer == _players[0] ? ePlayerTurn.Player1 : ePlayerTurn.Player2;

        public DartLeg(IDartCounter[] dartCounters)
        {
            if (dartCounters == null) throw new ArgumentException();
            if (dartCounters.Length != 2) throw new ArgumentException();

            _players = dartCounters;
            _currentPlayer = dartCounters[0];
        }

        public DartCounter.ThrowResult Throw(int value, int multiplier)
        {
            DartCounter.ThrowResult result = _currentPlayer.Throw(value, multiplier);
            ArrowCount--;

            if (result == DartCounter.ThrowResult.Dead || result == DartCounter.ThrowResult.Win || ArrowCount == 0)
            {
                SwitchPlayer(); 
            }
            
            return result;
        }

        private void SwitchPlayer()
        {
            ArrowCount = 3;
            _currentPlayer = _currentPlayer == _players[0] ? _players[1] : _players[0];
        }
    }
}